#include "StdAfx.h"
#include "ZZHLog.h"
#include <stdarg.h>
#include <windows.h>
#include <tchar.h>
#include <Shlwapi.h>
#include "log4cxx/propertyconfigurator.h"

#ifdef DEBUG
#pragma comment(lib, "log4cxxd.lib")
#else
#pragma comment(lib, "log4cxx.lib")
#endif

static pLogFun g_pLogFun = NULL;

std::string ZZH_MsgFromat(int iLevel, char *msg, ...)
{
	std::string strValue;
	do {
		va_list ap; 
		va_start(ap, msg); 
		size_t actSize = _vscprintf(msg, ap);
		std::shared_ptr<char> buf(new char[actSize+1]);
		memset(buf.get(), 0, actSize+1);
		int iLenFmt = vsnprintf(buf.get(), actSize+1, msg, ap); 
		va_end(ap); 
		strValue.assign(buf.get());
	} while (0);
	//转移日志-默认只输出到文件中
	if (g_pLogFun) {
		g_pLogFun(iLevel, strValue);
	}
	return strValue;
}
	
void ZZH_LogInit(char *configFile)
{
	//使用unicode支持目录有中文
	if (configFile == NULL)
	{
		wchar_t szModuleFileName[MAX_PATH];
		memset(szModuleFileName, 0, sizeof(wchar_t) * MAX_PATH);
		DWORD dwRet = GetModuleFileNameW(NULL, szModuleFileName, MAX_PATH);
		dwRet = PathRemoveFileSpecW(szModuleFileName);

		std::wstring strConfigPath = szModuleFileName + std::wstring(L"\\log4cxx.properties");
		log4cxx::PropertyConfigurator::configure(strConfigPath);
	}
	else
	{
		log4cxx::PropertyConfigurator::configure(configFile);
	}
	//log4cxx::PropertyConfigurator::configureAndWatch(strConfigPath, 3000);//动态监控变化
}

void ZZH_SetLogFun(pLogFun pFun)
{
	g_pLogFun = pFun;
}
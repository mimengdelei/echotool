
// echoToolDlg.h : header file
//

#pragma once
#include "afxwin.h"


// CechoToolDlg dialog
class CechoToolDlg : public CDialogEx
{
// Construction
public:
	CechoToolDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_ECHOTOOL_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	void RefreshLog();
	void StartRefreshTimer();
	void StopRefreshTimer();

	CEdit m_EditLog;
	BOOL m_bStopLog;
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonClearLog();
	afx_msg void OnBnClickedCheckStopLog();
	CIPAddressCtrl m_IPAddress;
	CEdit m_editLocalPort;
	CEdit m_editRemotePort;
};

#ifndef LOGHELP_H_
#define LOGHELP_H_

#include <log4cxx/logger.h>
#include <log4cxx/propertyconfigurator.h>
using namespace log4cxx;

#define msgFormatA(buf, msg) \
	do { \
	va_list ap; \
	va_start(ap, msg); \
	vsprintf_s(buf, sizeof(buf), msg, ap); \
	va_end(ap); \
	} while (0)

#define msgFormatW(buf, msg) \
	do { \
	va_list ap; \
	va_start(ap, msg); \
	vswprintf(buf, sizeof(buf), msg, ap); \
	va_end(ap); \
	} while (0)

class CLogHelper
{
public:
	static void Trace(LoggerPtr log, const char *msg, ...)
	{
		if (!log){
			return;
		}
		char buf[BUFFER_SIZE] = {0};
		msgFormatA(buf, msg);
		LOG4CXX_TRACE(log, buf);
	}

	static void Trace(LoggerPtr log, const wchar_t *msg, ...)
	{
		if (!log){
			return;
		}
		wchar_t buf[BUFFER_SIZE] = {0};
		msgFormatW(buf, msg);
		LOG4CXX_TRACE(log, Unicode2Ansi(buf));
	}

	static void Debug(LoggerPtr log, const char *msg, ...)
	{
		if (!log){
			return;
		}
		char buf[BUFFER_SIZE] = {0};
		msgFormatA(buf, msg);
		LOG4CXX_DEBUG(log, buf);
	}

	static void Debug(LoggerPtr log, const wchar_t *msg, ...)
	{
		if (!log){
			return;
		}
		wchar_t buf[BUFFER_SIZE] = {0};
		msgFormatW(buf, msg);
		LOG4CXX_DEBUG(log, Unicode2Ansi(buf));
	}

	static void Info(LoggerPtr log, const char *msg, ...)
	{
		if (!log){
			return;
		}
		char buf[BUFFER_SIZE] = {0};
		msgFormatA(buf, msg);
		LOG4CXX_INFO(log, buf);
	}

	static void Info(LoggerPtr log, const wchar_t *msg, ...)
	{
		if (!log){
			return;
		}
		wchar_t buf[BUFFER_SIZE] = {0};
		msgFormatW(buf, msg);
		LOG4CXX_INFO(log, Unicode2Ansi(buf));
	}

	static void Warn(LoggerPtr log, const char *msg, ...)
	{
		if (!log){
			return;
		}
		char buf[BUFFER_SIZE] = {0};
		msgFormatA(buf, msg);
		LOG4CXX_WARN(log, buf);
	}

	static void Warn(LoggerPtr log, const wchar_t *msg, ...)
	{
		if (!log){
			return;
		}
		wchar_t buf[BUFFER_SIZE] = {0};
		msgFormatW(buf, msg);
		LOG4CXX_WARN(log, Unicode2Ansi(buf));
	}

	static void Error(LoggerPtr log, const char *msg, ...)
	{
		if (!log){
			return;
		}
		char buf[BUFFER_SIZE] = {0};
		msgFormatA(buf, msg);
		LOG4CXX_ERROR(log, buf);
	}

	static void Error(LoggerPtr log, const wchar_t *msg, ...)
	{
		if (!log){
			return;
		}
		wchar_t buf[BUFFER_SIZE] = {0};
		msgFormatW(buf, msg);
		LOG4CXX_ERROR(log, Unicode2Ansi(buf));
	}

	static void Fatal(LoggerPtr log, const char *msg, ...)
	{
		if (!log){
			return;
		}
		char buf[BUFFER_SIZE] = {0};
		msgFormatA(buf, msg);
		LOG4CXX_FATAL(log, buf);
	}

	static void Fatal(LoggerPtr log, const wchar_t *msg, ...)
	{
		if (!log){
			return;
		}
		wchar_t buf[BUFFER_SIZE] = {0};
		msgFormatW(buf, msg);
		LOG4CXX_FATAL(log, Unicode2Ansi(buf));
	}
private:
	static std::string Unicode2Ansi(const std::wstring& unicode)
	{
		if (unicode.empty()) {
			return std::string("");
		}
		int len = WideCharToMultiByte(CP_ACP, 0, unicode.c_str(), -1, NULL, 0, NULL, NULL);
		char *tmp = new char[len+1];
		memset(tmp, 0, sizeof(char)*(len+1));
		WideCharToMultiByte(CP_ACP, 0, unicode.c_str(), (int)unicode.size(), tmp, len, NULL, NULL);
		std::string ret = tmp;
		delete [] tmp;
		return ret;
	}

	static const unsigned short BUFFER_SIZE = 4096;
};
#endif // LOGHELP_H_
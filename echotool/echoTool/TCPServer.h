#pragma once
#include "tcp/Socket.h"
#include <afxmt.h>
#include <map>
using namespace std;

class CTCPServer : public TCP::cSocket
{
public:
	CTCPServer();
	~CTCPServer();

	BOOL StartListen(DWORD dwBindIP, USHORT uPort);
	bool Init();
	bool Run();
	void Stop();
protected:
	CString GetErrMsg(DWORD dwErrCode);
	CString FormatIP(DWORD u32_IP);
	void CloseSockets();
	static DWORD thread_run(LPVOID lpParm);
	void LoopEvents();

	/************************virtual functions************************/
	virtual void OnAccept(SOCKET s, DWORD u32_IP, USHORT u16_Port);
	virtual void OnRead(SOCKET s, DWORD u32_IP, cMemory* ppi_RecvMem);
	virtual void OnClose(SOCKET s);
	/*****************************************************************/
	
	HANDLE m_hThread;
	BOOL m_bRunning;
	DWORD m_dwBindIP;
	USHORT m_uPort;
	CString m_strLocalIP;
	unsigned short m_u16LocalPort;
};
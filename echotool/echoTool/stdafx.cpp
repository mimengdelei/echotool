
// stdafx.cpp : source file that includes just the standard includes
// echoTool.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"


CString GetExePath()
{
	CString strExePath;
	TCHAR szModuleFileName[MAX_PATH];
	memset(szModuleFileName, 0, sizeof (TCHAR) * MAX_PATH);
	::GetModuleFileName(NULL,szModuleFileName, MAX_PATH);
	::PathRemoveFileSpec(szModuleFileName);

	strExePath = szModuleFileName;
	return strExePath;
}

CString CharArray2HexString(char *pData, int iLen)
{
	if (NULL == pData || iLen < 1) return CString("");

	CString strResult;
	CString strHex;
	for (int i=0; i<iLen; ++i){

		strHex.Format(_T("%02X "), (unsigned char)pData[i]);
		strResult.Append(strHex);
	}

	return strResult;
}
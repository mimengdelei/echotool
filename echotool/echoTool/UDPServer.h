#pragma once
#include "udp/CESocket.h"
#include <string>

//echotool upd server
//绑定本地端口
//开启数据读取线程

class CUDPServer:public CCESocket
{
public:
	CUDPServer(void);
	~CUDPServer(void);

	bool SetRemote(CString strIP, unsigned short u16Port);
	bool BindLocal(CString strIP, unsigned short u16Port);
	virtual bool OnReceive(char* buf, int len);

	bool Init();
	bool Run();
	bool Stop();

	bool StartReadDataThread();

	BOOL InitAddress(CString strIP, unsigned short u16Port, SOCKADDR_IN &Address);
	int Send(const char* buf, int len);
public:
	CString m_strLocalIP;
	unsigned short m_u16LocalPort;

	CString m_strRemoteIP;
	unsigned short m_u16RemotePort;
};


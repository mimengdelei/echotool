#pragma once
#include <string>
#include "inifile/inifile.h"

class TAppINIConfig
{
public:
	TAppINIConfig(void);
	~TAppINIConfig(void);

	bool Load(std::string strFile);
	bool Create(std::string strFile);

	bool GetLocalIP(std::string &strValue);
	bool SetLocalIP(std::string &strValue);

	bool GetLocalPort(unsigned short &iValue);
	bool SetLocalPort(unsigned short iValue);

	bool GetRemotePort(unsigned short &iValue);
	bool SetRemotePort(unsigned short iValue);

	bool GetTCPServer(std::string &strValue);
	bool GetUDPServer(std::string &strValue);
protected:
	inifile::IniFile m_iniFile;
};

extern TAppINIConfig theAppINIConfig;


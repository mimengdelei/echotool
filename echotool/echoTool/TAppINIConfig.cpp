#include "StdAfx.h"
#include "TAppINIConfig.h"

 TAppINIConfig theAppINIConfig;

TAppINIConfig::TAppINIConfig(void)
{
}


TAppINIConfig::~TAppINIConfig(void)
{
}

bool TAppINIConfig::Load(std::string strFile)
{
	int iRet = m_iniFile.Load(strFile);

	return RET_OK == iRet;
}
bool TAppINIConfig::Create(std::string strFile)
{
	int iRet = m_iniFile.Load(strFile);
	std::string strSection, strKey, strValue;

	strSection = "BASEINFO";
	strKey = "local_ip";
	strValue = "127.0.0.1";

	m_iniFile.SetStringValue(strSection, strKey, strValue);
	strKey = "local_port";
	strValue = "7";
	m_iniFile.SetStringValue(strSection, strKey, strValue);
	strKey = "remote_port";
	strValue = "7";
	m_iniFile.SetStringValue(strSection, strKey, strValue);

	strKey = "udp_server";
	strValue = "1";
	m_iniFile.SetStringValue(strSection, strKey, strValue);

	strKey = "tcp_server";
	strValue = "1";
	m_iniFile.SetStringValue(strSection, strKey, strValue);

	iRet = m_iniFile.SaveAs(strFile);
	return RET_OK == iRet;
}

bool TAppINIConfig::GetLocalIP(std::string &strValue)
{
	std::string strSection, strKey;

	strSection = "BASEINFO";
	strKey = "local_ip";

	int iRet = m_iniFile.GetStringValue(strSection, strKey, &strValue);
	return iRet == RET_OK;
}
bool TAppINIConfig::SetLocalIP(std::string &strValue)
{
	std::string strSection, strKey;

	strSection = "BASEINFO";
	strKey = "local_ip";

	int iRet = m_iniFile.SetStringValue(strSection, strKey, strValue);
	return iRet == RET_OK;
}

bool TAppINIConfig::GetLocalPort(unsigned short &iValue)
{
	std::string strSection, strKey, strValue;

	strSection = "BASEINFO";
	strKey = "local_port";

	int iRet = m_iniFile.GetStringValue(strSection, strKey, &strValue);
	iValue = atoi(strValue.c_str());
	return iRet == RET_OK;
}
bool TAppINIConfig::SetLocalPort(unsigned short iValue)
{
	std::string strSection, strKey, strValue;

	strSection = "BASEINFO";
	strKey = "local_port";

	int iRet = m_iniFile.SetIntValue(strSection, strKey, iValue);
	return iRet == RET_OK;
}

bool TAppINIConfig::GetRemotePort(unsigned short &iValue)
{
	std::string strSection, strKey, strValue;

	strSection = "BASEINFO";
	strKey = "remote_port";

	int iRet = m_iniFile.GetStringValue(strSection, strKey, &strValue);
	iValue = atoi(strValue.c_str());
	return iRet == RET_OK;
}

bool TAppINIConfig::SetRemotePort(unsigned short iValue)
{
	std::string strSection, strKey, strValue;

	strSection = "BASEINFO";
	strKey = "remote_port";

	int iRet = m_iniFile.SetIntValue(strSection, strKey, iValue);
	return iRet == RET_OK;
}

bool TAppINIConfig::GetTCPServer(std::string &strValue)
{
	std::string strSection, strKey;

	strSection = "BASEINFO";
	strKey = "tcp_server";

	int iRet = m_iniFile.GetStringValue(strSection, strKey, &strValue);

	return iRet == RET_OK;
}
bool TAppINIConfig::GetUDPServer(std::string &strValue)
{
	std::string strSection, strKey;

	strSection = "BASEINFO";
	strKey = "udp_server";

	int iRet = m_iniFile.GetStringValue(strSection, strKey, &strValue);
	return iRet == RET_OK;
}
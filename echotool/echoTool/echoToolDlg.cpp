
// echoToolDlg.cpp : implementation file
//

#include "stdafx.h"
#include "echoTool.h"
#include "echoToolDlg.h"
#include "afxdialogex.h"
#include "TAppINIConfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#pragma comment(lib,"Version.lib")

#define MAX_LOG_LINES				1000
#define MAX_EDIT_TEXT_LENGTH		25000	//edit最大长度
#define TIMER_ID_OF_REFRESH_LOG			10001
#define TIMER_ID_OF_REFRESH_SUB_MODULE_STATUS	10002

CString GetCompileDateTime()
{        
	const int  MONTH_PER_YEAR=12;
	const char szEnglishMonth[MONTH_PER_YEAR][4]={ "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	char szTmpDate[40]={0};
	char szTmpTime[20]={0};
	char szMonth[4]={0};
	int iYear,iMonth,iDay,iHour,iMin,iSec;

	sprintf_s(szTmpDate, 40, "%s",__DATE__);	// Sep 18 2017
	sprintf_s(szTmpTime, 20 ,"%s",__TIME__);    // 10:59:19

	sscanf(szTmpDate,"%s %d %d",szMonth,&iDay,&iYear);
	sscanf(szTmpTime,"%d:%d:%d",&iHour,&iMin,&iSec);

	for(int i = 0; MONTH_PER_YEAR; i++)
	{
		if(strncmp(szMonth,szEnglishMonth[i],3)==0)
		{
			iMonth = i+1;
			break;
		}
	}

	CString strDataTime;
	strDataTime.Format(_T("【编译时间%04d-%02d-%02d %02d:%02d:%02d】"),iYear,iMonth,iDay,iHour,iMin,iSec);
	return strDataTime;
}

CString GetProductionVersion()
{
	TCHAR szFullPath[MAX_PATH];
	DWORD dwVerInfoSize = 0;
	DWORD dwVerHnd;
	VS_FIXEDFILEINFO * pFileInfo;
	CString strVersion;

	GetModuleFileName(NULL, szFullPath, sizeof(szFullPath));
	dwVerInfoSize = GetFileVersionInfoSize(szFullPath, &dwVerHnd);
	if (dwVerInfoSize)
	{
		// If we were able to get the information, process it:
		HANDLE  hMem;
		LPVOID  lpvMem;
		unsigned int uInfoSize = 0;

		hMem = GlobalAlloc(GMEM_MOVEABLE, dwVerInfoSize);
		lpvMem = GlobalLock(hMem);
		GetFileVersionInfo(szFullPath, dwVerHnd, dwVerInfoSize, lpvMem);

		::VerQueryValue(lpvMem, (LPTSTR)_T("\\"), (void**)&pFileInfo, &uInfoSize);

		WORD wVersion[4];

		// Product version from the FILEVERSION of the version info resource 
		wVersion[0] = HIWORD(pFileInfo->dwProductVersionMS); 
		wVersion[1] = LOWORD(pFileInfo->dwProductVersionMS);
		wVersion[2] = HIWORD(pFileInfo->dwProductVersionLS);
		wVersion[3] = LOWORD(pFileInfo->dwProductVersionLS); 
		strVersion.Format(_T("%d.%d.%d.%d"),wVersion[0],wVersion[1],wVersion[2],wVersion[3]);
		GlobalUnlock(hMem);
		GlobalFree(hMem);
	}
	return strVersion;
}

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CechoToolDlg dialog




CechoToolDlg::CechoToolDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CechoToolDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_bStopLog = false;
}

void CechoToolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_LOG, m_EditLog);
	DDX_Control(pDX, IDC_EDIT_LOCAL_IP, m_IPAddress);
	DDX_Control(pDX, IDC_EDIT_LOCAL_PORT, m_editLocalPort);
	DDX_Control(pDX, IDC_EDIT_REMOTE_PORT, m_editRemotePort);
}

BEGIN_MESSAGE_MAP(CechoToolDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_CLEAR_LOG, &CechoToolDlg::OnBnClickedButtonClearLog)
	ON_BN_CLICKED(IDC_CHECK_STOP_LOG, &CechoToolDlg::OnBnClickedCheckStopLog)
END_MESSAGE_MAP()


// CechoToolDlg message handlers

BOOL CechoToolDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	// 设置窗口标题
	CString strWindowText;
	strWindowText.Format(_T("【Echo Tool】【V%s】【%s】"), 
		GetProductionVersion(),
		GetCompileDateTime()
		);
	SetWindowText(strWindowText);

	std::string strIP;
	unsigned short u16LocalPort;
	unsigned short u16RemotePort;

	theAppINIConfig.GetLocalIP(strIP);
	theAppINIConfig.GetLocalPort(u16LocalPort);
	theAppINIConfig.GetLocalPort(u16RemotePort);

	m_IPAddress.SetWindowText(strIP.c_str());

	CString strText;
	strText.Format(_T("%d"),u16LocalPort);
	m_editLocalPort.SetWindowText(strText);

	strText.Format(_T("%d"),u16RemotePort);
	m_editRemotePort.SetWindowText(strText);

	CheckDlgButton(IDC_CHECK_STOP_LOG, m_bStopLog);
	StartRefreshTimer();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CechoToolDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CechoToolDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CechoToolDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void  CechoToolDlg::RefreshLog()
{	
	if (m_bStopLog) return;
	int iLineCount = m_EditLog.GetLineCount();
	if (iLineCount >= MAX_LOG_LINES){
		m_EditLog.SetWindowText(_T(""));
	}
	iLineCount = m_EditLog.GetWindowTextLength();
	if (iLineCount >= MAX_EDIT_TEXT_LENGTH)
	{
		m_EditLog.SetWindowText(_T(""));
	}

	std::queue<std::string> queueLog;
	if (theApp.GetLogLine(queueLog))
	{
		StopRefreshTimer();
		std::string strLine;
		while (!queueLog.empty())
		{
			strLine += queueLog.front();
			strLine += "\r\n";
			queueLog.pop();
		}	
		int length = m_EditLog.GetWindowTextLength();
		m_EditLog.SetSel(length, length);
		m_EditLog.ReplaceSel(strLine.c_str());
		StartRefreshTimer();
	}
}
void CechoToolDlg::StartRefreshTimer()
{
	SetTimer(TIMER_ID_OF_REFRESH_LOG, 500, NULL);
}
void CechoToolDlg::StopRefreshTimer()
{
	KillTimer(TIMER_ID_OF_REFRESH_LOG);
}

void CechoToolDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (TIMER_ID_OF_REFRESH_LOG == nIDEvent)
		RefreshLog();
	CDialogEx::OnTimer(nIDEvent);
}


void CechoToolDlg::OnBnClickedButtonClearLog()
{
	// TODO: 在此添加控件通知处理程序代码
	m_EditLog.SetWindowText(_T(""));
}


void CechoToolDlg::OnBnClickedCheckStopLog()
{
	// TODO: 在此添加控件通知处理程序代码
	//m_bStopLog = TRUE;
	m_bStopLog = this->IsDlgButtonChecked(IDC_CHECK_STOP_LOG);
}

#include "StdAfx.h"
#include "UDPServer.h"
#include "TAppINIConfig.h"
#include "echoTool.h"
#include "ZZHLog.h"

CUDPServer::CUDPServer(void)
{
}


CUDPServer::~CUDPServer(void)
{
}

bool CUDPServer::SetRemote(CString strIP, unsigned short u16Port)
{
	return false;
}
bool CUDPServer::BindLocal(CString strIP, unsigned short u16Port)
{
	if (!InitAddress(m_strLocalIP, m_u16LocalPort, m_localAddress))
	{
		m_errorCode = WSAGetLastError();
		ZZH_LOG_ERROR(LOGNAME,"bind IP or port failed,error code=%d,IP=%s,Port=%d", 
			m_errorCode, strIP, u16Port);
		return false;
	}

	if (bind(s, (SOCKADDR*) &m_localAddress, sizeof(SOCKADDR_IN)) == SOCKET_ERROR)
	{
		m_errorCode = WSAGetLastError();
		ZZH_LOG_ERROR(LOGNAME, "bind IP or port failed,error code=%d,IP=%s,Port=%d", 
			m_errorCode, strIP, u16Port);
		return false;
	}
	ZZH_LOG_ERROR(LOGNAME, "start udp server success,IP=%s,Port=%d", strIP, u16Port);
	return true;
}
bool CUDPServer::OnReceive(char* buf, int len)
{		
	CString strHex;
	strHex = CharArray2HexString(buf, len);

	m_udpReadyToSend = TRUE;
	m_socketState = CONNECTED;
	m_strRemoteIP.Format("%s", (char *)inet_ntoa(m_localAddress.sin_addr));
	m_u16RemotePort = htons(m_localAddress.sin_port);

	CString strText;

	strText.Format("[IP=%s,Port=%d]data=%s", m_strRemoteIP, m_u16RemotePort, strHex);

	ZZH_LOG_INFO(LOGNAME, "[UDP]Receive%s", strText);

	InitAddress(m_strRemoteIP, m_u16RemotePort, m_remoteAddress);
	Send(buf, len);
	ZZH_LOG_INFO(LOGNAME, "[UDP]Send%s", strText);
	return true;
}
bool CUDPServer::Init()
{
	bool bRet = Create(SOCK_DGRAM);
	std::string strValue;

	theAppINIConfig.GetLocalIP(strValue);
	m_strLocalIP = strValue.c_str();
	theAppINIConfig.GetLocalPort(m_u16LocalPort);
	
	BindLocal(m_strLocalIP, m_u16LocalPort);

	return true;
}
bool CUDPServer::Run()
{
	StartReadDataThread();
	return true;
}
bool CUDPServer::Stop()
{
	return true;
}

bool CUDPServer::StartReadDataThread()
{
	if (m_readThreadState == CLOSED)
	{
		m_readThreadState = RUNNING;
		m_readThread = CreateThread(NULL, 0, StartThread, this, 0, NULL );
	}
	return true;
}

BOOL CUDPServer::InitAddress(CString strIP, unsigned short u16Port, SOCKADDR_IN &Address)
{
	char hostStr[257];
	int wHostLen;
	LPTSTR wHost;
	ulong hostByIP;
	HOSTENT *hostByName;

	//Check port values
	if(u16Port > 65535){
		m_errorCode = WSAEINVAL;
		return FALSE;
	}

	//Gets address string and convert it from unicode to multibyte
	wHostLen = strIP.GetLength();
	if(wHostLen > 256) wHostLen = 256;
	wHost = (char*)strIP.GetBuffer(wHostLen);
#ifdef _WIN32_WCE
	wcstombs(hostStr, wHost, 256);
#else
	wHost[wHostLen] = '\0';
	#ifdef _UNICODE
		int iLength = WideCharToMultiByte (CP_ACP, 0, addr, -1, NULL, 0, NULL, NULL);
		WideCharToMultiByte(CP_ACP, 0, addr, -1, hostStr, iLength, NULL, NULL);
		hostStr[iLength] = '\0';
	#else
		strcpy(hostStr, wHost);
	#endif

#endif

	//Builds destination address
	memset(&Address, 0, sizeof(SOCKADDR_IN));
	Address.sin_family = AF_INET;
	Address.sin_port = htons(u16Port);

	hostByIP = inet_addr(hostStr);
	if (hostByIP == INADDR_NONE)
	{
		hostByName = gethostbyname(hostStr);
		if(hostByName == NULL){
			m_errorCode = WSAGetLastError();
			strIP.ReleaseBuffer();
			return FALSE;
		}
		Address.sin_addr = *((IN_ADDR*)hostByName->h_addr_list[0]);
		strIP.ReleaseBuffer();
		return TRUE;
	}

	Address.sin_addr.s_addr = hostByIP;
	strIP.ReleaseBuffer();
	return TRUE; 
}

int CUDPServer::Send(const char* buf, int len)
{
	return __super::Send(buf, len);
}
#pragma once
#include <string>
#include "log4cxx/logger.h"
#include "log4cxx/level.h"

#define ZZH_LOG_NAME(logName) logName
typedef void (*pLogFun)(int logLevel, std::string &strInfo);

extern std::string ZZH_MsgFromat(int iLevel, char *msg, ...);
extern void ZZH_LogInit(char *configFile);
extern void ZZH_SetLogFun(pLogFun pFun);
/**
Logs a message to a specified logger with a specified level.

@param logger the logger to be used.
@param level the level to log.
@param message the message string to log.
*/
#define ZZH_LOG(logName, level, format, ...) { \
	log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger(ZZH_LOG_NAME(logName));\
	if (logger){\
	::log4cxx::LevelPtr levelPtr= ::log4cxx::Level::toLevel(level);\
	if (logger->isEnabledFor(levelPtr)) {\
	std::string message = ZZH_MsgFromat (level, format, ##__VA_ARGS__);  \
	::log4cxx::helpers::MessageBuffer oss_; \
	logger->forcedLog(levelPtr, oss_.str(oss_ << message), LOG4CXX_LOCATION); } }}

#define ZZH_LOG_DEBUG(logName, format, ...) { \
	log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger(ZZH_LOG_NAME(logName));\
	if (logger){\
	if (LOG4CXX_UNLIKELY(logger->isDebugEnabled())) {\
	std::string message = ZZH_MsgFromat (log4cxx::Level::DEBUG_INT, format, ##__VA_ARGS__);  \
	::log4cxx::helpers::MessageBuffer oss_; \
	logger->forcedLog(::log4cxx::Level::getDebug(), oss_.str(oss_ << message), LOG4CXX_LOCATION); }}}

#define ZZH_LOG_TRACE(logName, format, ...) { \
	log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger(ZZH_LOG_NAME(logName));\
	if (logger){\
	if (LOG4CXX_UNLIKELY(logger->isTraceEnabled())) {\
	std::string message = ZZH_MsgFromat (log4cxx::Level::TRACE_INT, format, ## __VA_ARGS__);  \
	::log4cxx::helpers::MessageBuffer oss_; \
	logger->forcedLog(::log4cxx::Level::getTrace(), oss_.str(oss_ << message), LOG4CXX_LOCATION); }}}

#define ZZH_LOG_INFO(logName, format, ...) { \
	log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger(ZZH_LOG_NAME(logName));\
	if (logger){\
	if (LOG4CXX_UNLIKELY(logger->isInfoEnabled())) {\
	std::string message = ZZH_MsgFromat (log4cxx::Level::INFO_INT, format, ## __VA_ARGS__);  \
	::log4cxx::helpers::MessageBuffer oss_; \
	logger->forcedLog(::log4cxx::Level::getInfo(), oss_.str(oss_ << message), LOG4CXX_LOCATION); }}}

#define ZZH_LOG_WARN(logName, format, ...) { \
	log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger(ZZH_LOG_NAME(logName));\
	if (logger){\
	if (LOG4CXX_UNLIKELY(logger->isWarnEnabled())) {\
	std::string message = ZZH_MsgFromat (log4cxx::Level::WARN_INT, format, ## __VA_ARGS__);  \
	::log4cxx::helpers::MessageBuffer oss_; \
	logger->forcedLog(::log4cxx::Level::getWarn(), oss_.str(oss_ << message), LOG4CXX_LOCATION); }}}

#define ZZH_LOG_ERROR(logName, format, ...) { \
	log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger(ZZH_LOG_NAME(logName));\
	if (logger){\
	if (LOG4CXX_UNLIKELY(logger->isErrorEnabled())) {\
	std::string message = ZZH_MsgFromat (log4cxx::Level::ERROR_INT, format, ## __VA_ARGS__);  \
	::log4cxx::helpers::MessageBuffer oss_; \
	logger->forcedLog(::log4cxx::Level::getError(), oss_.str(oss_ << message), LOG4CXX_LOCATION); }}}
// AutoLock.h: interface for the CAutoLock class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AUTOLOCK_H__E02D2994_04AA_4489_A245_ECAA9EB188C9__INCLUDED)
#define AFX_AUTOLOCK_H__E02D2994_04AA_4489_A245_ECAA9EB188C9__INCLUDED

#include <afxmt.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//======================================================================
// 类型名称:CAutoLock
// 功能描述:临界区的自动锁定和解锁
// 创建日期:2006-03-27
// 修改日期:
// 作    者:胡鹏武
// 备    注:利用对象生存期的特性进行自动解锁和锁定
//			根据对象的生存期的原理,在对象的生成阶段进行临界区锁定的操作;
//			在对象的销毁阶段进行临界区解锁的操作;
//	
// 示    例:
//			BOOL CQueueControlChange::GetControl(CControl &ctrlControl)
//			{
//				// 通过m_Lock.m_strName可以检查死锁的位置
//				CAutoLock AutoLock(m_Lock,"CQueueControlChange");
//
//				int nSize = m_arrControl.GetSize();
//				if (nSize<=0){
//					return FALSE;
//				}
//
//				ctrlControl = m_arrControl[0];
//				
//				m_arrControl.RemoveAt(0);
//
//				return TRUE;
//			}	
//======================================================================
class WinLockerBase
{
public:
	WinLockerBase() {}
	virtual ~WinLockerBase() {};

public:
	virtual void Lock() = 0;
	virtual void Unlock() = 0;
	void SetName(const CString& name) { m_name = name; }
protected:
	CString m_name;
};

/** 互斥锁  **/
class MutexLocker : public WinLockerBase
{
public:
	MutexLocker();
	virtual ~MutexLocker();

private:
	MutexLocker(MutexLocker const&);
	MutexLocker& operator=(MutexLocker const&);
	HANDLE m_hMutex;

public:
	virtual void Lock();
	virtual void Unlock();
};
/** 临界区 **/

class CriticalSectionLocker : public WinLockerBase
{
public:
	CriticalSectionLocker();
	virtual ~CriticalSectionLocker();

private:
	CriticalSectionLocker(CriticalSectionLocker const&);
	CriticalSectionLocker& operator=(CriticalSectionLocker const&);
	CRITICAL_SECTION m_cs;

public:
	virtual void Lock();
	virtual void Unlock();
};

/** 自动锁 **/
class CAutoLock
{
public:
	CAutoLock(CSyncObject& pLocker, bool bNeedConstructLock=true);
	CAutoLock(CSyncObject& pLocker, const CString& strFunName, bool bNeedConstructLock=true);
	CAutoLock(WinLockerBase& pLocker, bool bNeedConstructLock=true);
	CAutoLock(WinLockerBase& pLocker, const CString& strFunName, bool bNeedConstructLock=true);
	virtual ~CAutoLock();
	
private:
	void Lock();
	void Unlock();
private:
	/** MFC锁**/
	CSyncObject*		m_pLocker;
	/** Win32锁 **/
	WinLockerBase*      m_pLocker1;
	/** 用来判断是否释放，true表示释放，false表示未释放 **/
	bool				m_bUnlock;
	/** 判断是MFC锁还是Win32锁，false表示MFC锁，true表示Win32锁 **/
	bool				m_bWinMfc;
};

#endif // !defined(AFX_AUTOLOCK_H__E02D2994_04AA_4489_A245_ECAA9EB188C9__INCLUDED_)

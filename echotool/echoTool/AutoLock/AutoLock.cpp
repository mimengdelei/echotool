// AutoLock.cpp: implementation of the CAutoLock class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AutoLock.h"
//#include "../LogMgmt/LogPrint.h"
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/** 互斥锁 **/
MutexLocker::MutexLocker()
{
	m_hMutex =  CreateMutex(NULL, FALSE, NULL);
	if(NULL == m_hMutex)
	{
		;//ZZH_LOG_ERROR(LibCurlHttp,  "Create mutex locker fail.");
	}
}

MutexLocker::~MutexLocker()
{
	CloseHandle(m_hMutex);
	m_hMutex = NULL;
}

void MutexLocker::Lock()
{
	DWORD res = WaitForSingleObject(m_hMutex, INFINITE);
	if(WAIT_OBJECT_0 != res)
	{
		;//ZZH_LOG_ERROR(LibCurlHttp,  "Can not lock locker.");
	}
}

void MutexLocker::Unlock()
{
	if(TRUE != ReleaseMutex(m_hMutex))
	{
		;//ZZH_LOG_ERROR(LibCurlHttp,  "Release locker fail.");
	}
}

/** 临界区锁 **/
CriticalSectionLocker::CriticalSectionLocker()
{
	InitializeCriticalSection(&m_cs);
}

CriticalSectionLocker::~CriticalSectionLocker()
{
	DeleteCriticalSection(&m_cs);
}

void CriticalSectionLocker::Lock()
{
	EnterCriticalSection(&m_cs);
}

void CriticalSectionLocker::Unlock()
{
	LeaveCriticalSection(&m_cs);
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//======================================================================
// 函数名称:CAutoLock()
// 功能描述:构造函数
// 输入参数:pLocker=锁
// 输出参数:
// 返 回 值:
// 创建日期:2006-03-27
// 修改日期:2019-05-23(李俊贤)
// 作    者:胡鹏武
// 备    注:根据对象的生存期的原理,在对象的生成阶段进行锁定的操作	
//======================================================================
CAutoLock::CAutoLock(CSyncObject& pLocker,bool bNeedConstructLock)
	:m_pLocker(&pLocker),m_bUnlock(false),m_bWinMfc(false)
{
	if(bNeedConstructLock)
	{
		m_pLocker->Lock();
	}
}

//======================================================================
// 函数名称:CAutoLock()
// 功能描述:构造函数
// 输入参数:pLocker=锁
//			strFunName=函数标记
// 输出参数:
// 返 回 值:
// 创建日期:2006-03-27
// 修改日期:2019-05-23(李俊贤)
// 作    者:胡鹏武
// 备    注:<1> 根据对象的生存期的原理,在对象的生成阶段进行解锁的操作	
//			<2> 当死锁发生的时候,csLock的m_strName成员可以知道死锁的位置
//======================================================================
CAutoLock::CAutoLock(CSyncObject& pLocker, const CString& strFunName, bool bNeedConstructLock)
	:m_pLocker(&pLocker),m_bUnlock(false),m_bWinMfc(false)
{
	if(bNeedConstructLock)
	{
		m_pLocker->Lock();
	}

	
#ifdef _DEBUG
	// 该成员只在调试版本中进行了定义
	m_pLocker->m_strName = strFunName;
#endif
	
}

CAutoLock::CAutoLock(WinLockerBase& pLocker, bool bNeedConstructLock)
	:m_pLocker1(&pLocker),m_bUnlock(false),m_bWinMfc(true)
{
	if(bNeedConstructLock)
	{
		m_pLocker1->Lock();
	}
}
CAutoLock::CAutoLock(WinLockerBase& pLocker, const CString& strFunName, bool bNeedConstructLock)
	:m_pLocker1(&pLocker),m_bUnlock(false),m_bWinMfc(true)
{
	if(bNeedConstructLock)
	{
		m_pLocker1->Lock();
	}


#ifdef _DEBUG
	m_pLocker1->SetName(strFunName);
#endif
}
//======================================================================
// 函数名称:~CAutoLock()
// 功能描述:析构函数
// 输入参数:
// 输出参数:
// 返 回 值:
// 创建日期:2006-03-27
// 修改日期:2019-05-23(李俊贤)
// 作    者:胡鹏武
// 备    注:根据对象的生存期的原理,在对象的销毁阶段进行解锁的操作	
//======================================================================
CAutoLock::~CAutoLock()
{
	this->Unlock();
}

//======================================================================
// 函数名称:Lock()
// 功能描述:加锁函数
// 输入参数:
// 输出参数:
// 返 回 值:
// 创建日期:2019-05-23
// 修改日期:
// 作    者:李俊贤
// 备    注:该函数是在同一个函数中调用Unlock后需要再次加锁情况下调用	
//======================================================================
void CAutoLock::Lock()
{
	if(!m_bUnlock) return;
	m_bUnlock = false;
	if(!m_bWinMfc)
	{
		m_pLocker->Lock();
		return;
	}
	m_pLocker1->Lock();
}

//======================================================================
// 函数名称:Unlock()
// 功能描述:解锁函数
// 输入参数:
// 输出参数:
// 返 回 值:
// 创建日期:2019-05-23
// 修改日期:
// 作    者:李俊贤
// 备    注:该函数是在同一个函数中需要中间解锁情况下调用	
//======================================================================
void CAutoLock::Unlock()
{
	if(m_bUnlock) return;
	m_bUnlock = true;
	if(!m_bWinMfc)
	{
		m_pLocker->Unlock();
		return;
	}
	m_pLocker1->Unlock();
	
}

// echoTool.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include <queue>
#include <string>
#include "UDPServer.h"
#include "TCPServer.h"

// CechoToolApp:
// See echoTool.cpp for the implementation of this class
//

class CechoToolApp : public CWinApp
{
public:
	CechoToolApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()

public:
	BOOL GetLogLine(std::queue<std::string> &m_strLogQueue);
	BOOL OutputLog(std::string strLine);
	static void Log(int logLevel, std::string &strInfo);
protected:
	std::queue<std::string> m_strLogQueue;
	CCriticalSection m_csLogQueue;
	BOOL m_bExitApp;

	CUDPServer m_CUDPServer;
	CTCPServer m_CTCPServer;
};

extern CechoToolApp theApp;
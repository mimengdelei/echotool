//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by echoTool.rc
//
#define IDCanel                         2
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_ECHOTOOL_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDC_RADIO_TCP                   1000
#define IDC_RADIO_UDP                   1002
#define IDC_EDIT_LOCAL_PORT             1003
#define IDC_BUTTON_START_SERVER         1004
#define IDC_EDIT_LOG                    1005
#define IDC_EDIT_LOCAL_IP               1006
#define IDC_CHECK_STOP_LOG              1007
#define IDC_BUTTON_CLEAR_LOG            1008
#define IDC_EDIT_REMOTE_PORT            1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

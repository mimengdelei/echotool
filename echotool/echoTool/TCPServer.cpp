#include "stdafx.h"
#include "TCPServer.h"
#include <winsock2.h>
//#include "common/PackData.h"
#include <mstcpip.h>
#include "ZZHLog.h"
#include "TAppINIConfig.h"

CString CTCPServer::GetErrMsg(DWORD dwErrCode)
{
	CString s_Code;
	switch (dwErrCode)
	{
	case WSAEINTR:                s_Code = _T("WSAEINTR"); break;
	case WSAEBADF:                s_Code = _T("WSAEBADF"); break;
	case WSAEACCES:               s_Code = _T("WSAEACCES"); break;
	case WSAEFAULT:               s_Code = _T("WSAEFAULT"); break;
	case WSAEINVAL:               s_Code = _T("WSAEINVAL"); break;
	case WSAEMFILE:               s_Code = _T("WSAEMFILE"); break;
	case WSAEWOULDBLOCK:          s_Code = _T("WSAEWOULDBLOCK"); break;
	case WSAEINPROGRESS:          s_Code = _T("WSAEINPROGRESS"); break;
	case WSAEALREADY:             s_Code = _T("WSAEALREADY"); break;
	case WSAENOTSOCK:             s_Code = _T("WSAENOTSOCK"); break;
	case WSAEDESTADDRREQ:         s_Code = _T("WSAEDESTADDRREQ"); break;
	case WSAEMSGSIZE:             s_Code = _T("WSAEMSGSIZE"); break;
	case WSAEPROTOTYPE:           s_Code = _T("WSAEPROTOTYPE"); break;
	case WSAENOPROTOOPT:          s_Code = _T("WSAENOPROTOOPT"); break;
	case WSAEPROTONOSUPPORT:      s_Code = _T("WSAEPROTONOSUPPORT"); break;
	case WSAESOCKTNOSUPPORT:      s_Code = _T("WSAESOCKTNOSUPPORT"); break;
	case WSAEOPNOTSUPP:           s_Code = _T("WSAEOPNOTSUPP"); break;
	case WSAEPFNOSUPPORT:         s_Code = _T("WSAEPFNOSUPPORT"); break;
	case WSAEAFNOSUPPORT:         s_Code = _T("WSAEAFNOSUPPORT"); break;
	case WSAEADDRINUSE:           s_Code = _T("WSAEADDRINUSE"); break;
	case WSAEADDRNOTAVAIL:        s_Code = _T("WSAEADDRNOTAVAIL"); break;
	case WSAENETDOWN:             s_Code = _T("WSAENETDOWN"); break;
	case WSAENETUNREACH:          s_Code = _T("WSAENETUNREACH"); break;
	case WSAENETRESET:            s_Code = _T("WSAENETRESET"); break;
	case WSAECONNABORTED:         s_Code = _T("WSAECONNABORTED"); break;
	case WSAECONNRESET:           s_Code = _T("WSAECONNRESET"); break;
	case WSAENOBUFS:              s_Code = _T("WSAENOBUFS"); break;
	case WSAEISCONN:              s_Code = _T("WSAEISCONN"); break;
	case WSAENOTCONN:             s_Code = _T("WSAENOTCONN"); break;
	case WSAESHUTDOWN:            s_Code = _T("WSAESHUTDOWN"); break;
	case WSAETOOMANYREFS:         s_Code = _T("WSAETOOMANYREFS"); break;
	case WSAETIMEDOUT:            s_Code = _T("WSAETIMEDOUT"); break;
	case WSAECONNREFUSED:         s_Code = _T("WSAECONNREFUSED"); break;
	case WSAELOOP:                s_Code = _T("WSAELOOP"); break;
	case WSAENAMETOOLONG:         s_Code = _T("WSAENAMETOOLONG"); break;
	case WSAEHOSTDOWN:            s_Code = _T("WSAEHOSTDOWN"); break;
	case WSAEHOSTUNREACH:         s_Code = _T("WSAEHOSTUNREACH"); break;
	case WSAENOTEMPTY:            s_Code = _T("WSAENOTEMPTY"); break;
	case WSAEPROCLIM:             s_Code = _T("WSAEPROCLIM"); break;
	case WSAEUSERS:               s_Code = _T("WSAEUSERS"); break;
	case WSAEDQUOT:               s_Code = _T("WSAEDQUOT"); break;
	case WSAESTALE:               s_Code = _T("WSAESTALE"); break;
	case WSAEREMOTE:              s_Code = _T("WSAEREMOTE"); break;
	case WSASYSNOTREADY:          s_Code = _T("WSASYSNOTREADY"); break;
	case WSAVERNOTSUPPORTED:      s_Code = _T("WSAVERNOTSUPPORTED"); break;
	case WSANOTINITIALISED:       s_Code = _T("WSANOTINITIALISED"); break;
	case WSAEDISCON:              s_Code = _T("WSAEDISCON"); break;
	case WSAENOMORE:              s_Code = _T("WSAENOMORE"); break;
	case WSAECANCELLED:           s_Code = _T("WSAECANCELLED"); break;
	case WSAEINVALIDPROCTABLE:    s_Code = _T("WSAEINVALIDPROCTABLE"); break;
	case WSAEINVALIDPROVIDER:     s_Code = _T("WSAEINVALIDPROVIDER"); break;
	case WSAEPROVIDERFAILEDINIT:  s_Code = _T("WSAEPROVIDERFAILEDINIT"); break;
	case WSASYSCALLFAILURE:       s_Code = _T("WSASYSCALLFAILURE"); break;
	case WSASERVICE_NOT_FOUND:    s_Code = _T("WSASERVICE_NOT_FOUND"); break;
	case WSATYPE_NOT_FOUND:       s_Code = _T("WSATYPE_NOT_FOUND"); break;
	case WSA_E_NO_MORE:           s_Code = _T("WSA_E_NO_MORE"); break;
	case WSA_E_CANCELLED:         s_Code = _T("WSA_E_CANCELLED"); break;
	case WSAEREFUSED:             s_Code = _T("WSAEREFUSED"); break;
	case WSAHOST_NOT_FOUND:       s_Code = _T("WSAHOST_NOT_FOUND"); break;
	case WSATRY_AGAIN:            s_Code = _T("WSATRY_AGAIN"); break;
	case WSANO_RECOVERY:          s_Code = _T("WSANO_RECOVERY"); break;
	case WSANO_DATA:              s_Code = _T("WSANO_DATA"); break;
	case WSA_IO_PENDING:          s_Code = _T("WSA_IO_PENDING"); break;
	case WSA_IO_INCOMPLETE:       s_Code = _T("WSA_IO_INCOMPLETE"); break;
	case WSA_INVALID_HANDLE:      s_Code = _T("WSA_INVALID_HANDLE"); break;
	case WSA_INVALID_PARAMETER:   s_Code = _T("WSA_INVALID_PARAMETER"); break;
	case WSA_NOT_ENOUGH_MEMORY:   s_Code = _T("WSA_NOT_ENOUGH_MEMORY"); break;
	case WSA_OPERATION_ABORTED:   s_Code = _T("WSA_OPERATION_ABORTED"); break;

	default:
		s_Code.Format(_T("Code %u"), dwErrCode); 
		break;
	}

	CString s_Out;
	const DWORD BUFLEN = 1000;
	TCHAR t_Buf[BUFLEN];

	if (FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, dwErrCode, 0, t_Buf, BUFLEN, 0))
		s_Out.Format(_T("%s: %s"), s_Code, t_Buf);
	else 
		s_Out.Format(_T("%s: Windows has no explanation for this error"), s_Code);

	s_Out.TrimRight();
	return s_Out;
}

CString CTCPServer::FormatIP(DWORD u32_IP)
{
	BYTE* pu8_Addr = (BYTE*)&u32_IP;
	CString s_IP;
	s_IP.Format(_T("%d.%d.%d.%d"), pu8_Addr[0],pu8_Addr[1],pu8_Addr[2],pu8_Addr[3]);
	return s_IP;
}

CTCPServer::CTCPServer()
{
	m_bRunning = FALSE;
}

CTCPServer::~CTCPServer()
{

}

void CTCPServer::CloseSockets()
{
	if (GetSocketCount())
	{
		Close();
		ZZH_LOG_DEBUG(LOGNAME, _T("Socket(s) closed."));
	}
}
bool CTCPServer::Init()
{
	std::string strValue;
	theAppINIConfig.GetLocalIP(strValue);
	m_strLocalIP = strValue.c_str();
	theAppINIConfig.GetLocalPort(m_u16LocalPort);
	return true;
}
BOOL CTCPServer::StartListen(DWORD dwBindIP, USHORT uPort)
{
	m_dwBindIP = dwBindIP;
	m_uPort = uPort;

	if (GetSocketCount())
	{
		ZZH_LOG_DEBUG(LOGNAME, _T("Socket already in use"));
		return FALSE;
	}

	DWORD u32_Err = Listen(m_dwBindIP, m_uPort, INFINITE);

	if (u32_Err) 
	{
		ZZH_LOG_DEBUG(LOGNAME, _T("Listen Error %s"),GetErrMsg(u32_Err));
		CloseSockets();
		return FALSE;
	}

	ZZH_LOG_DEBUG(LOGNAME,_T("start tcp server success, listening (%s) on Port=%d...(waiting for FD_ACCEPT)."),
		m_dwBindIP == 0? _T("all network adapters") : FormatIP(m_dwBindIP), m_uPort);

	if (!m_bRunning)
	{
		m_bRunning = TRUE;
		m_hThread = CreateThread(0, 0, thread_run, this, 0, NULL);
	}

	return TRUE;
}

DWORD CTCPServer::thread_run(LPVOID lpParm)
{
	CTCPServer* pThis = (CTCPServer*)lpParm;
	pThis->LoopEvents();
	return 0;
}

void CTCPServer::LoopEvents()
{ 
	while (m_bRunning)
	{
		DWORD dwErrCode = ProcessEvents();
	}
}

void CTCPServer::OnAccept(SOCKET s, DWORD u32_IP, USHORT u16_Port)
{
	ZZH_LOG_INFO(LOGNAME, _T("accept connect socket=%d,IP=%s,Port=%d"),s,FormatIP(u32_IP), u16_Port);

	BOOL bKeepAlive = TRUE;
	int nRet = setsockopt(s, SOL_SOCKET, SO_KEEPALIVE,(char*)&bKeepAlive, sizeof(bKeepAlive));
	if (nRet == SOCKET_ERROR)
	{
		ZZH_LOG_ERROR(LOGNAME,_T("accept connect socket=%d,IP=%s,Port=%d, set SO_KEEPALIVE failed"),s,FormatIP(u32_IP), u16_Port);
		return;
	}
	tcp_keepalive inKeepalive;
	tcp_keepalive outKeepalive;
	inKeepalive.keepalivetime = 5*1000;
	inKeepalive.keepaliveinterval = 1000;
	inKeepalive.onoff = TRUE;

	unsigned long ulByteReturn = 0;
	nRet = WSAIoctl(s, SIO_KEEPALIVE_VALS,
		&inKeepalive, sizeof(inKeepalive),
		&outKeepalive, sizeof(outKeepalive),
		&ulByteReturn, NULL, NULL);
	if (nRet == SOCKET_ERROR)
	{
		ZZH_LOG_ERROR(LOGNAME, _T("accept connect socket=%d,IP=%s,Port=%d,set SIO_KEEPALIVE_VALS failed"),s,FormatIP(u32_IP), u16_Port);
		return;
	}
	ZZH_LOG_INFO(LOGNAME, _T("accept connect socket=%d,IP=%s,Port=%d success"),s,FormatIP(u32_IP), u16_Port);
}

void CTCPServer::OnRead(SOCKET s, DWORD u32_IP, cMemory* pi_RecvMem)
{
	SOCKADDR_IN clientAddr;
	int iLen(0);
	getpeername(s,(struct sockaddr *)&clientAddr, &iLen);
	CString strIP = FormatIP(u32_IP);
	while (TRUE)
	{
		char* s8_Buf = pi_RecvMem->GetBuffer();
		DWORD u32_Len = pi_RecvMem->GetLength();

		CString strText;
		CString strHex;
		strHex = CharArray2HexString(s8_Buf, u32_Len);

		strText.Format("[IP=%s,Port=%d]data=%s", strIP, htons(clientAddr.sin_port), strHex);

		ZZH_LOG_INFO(LOGNAME, "[TCP]Receive%s", strText);
		SendTo(s, s8_Buf, u32_Len);
		ZZH_LOG_INFO(LOGNAME, "[TCP]Send%s", strText);
		// Only delete the data that has been processed and leave the rest.
		// ATTENTION: DeleteLeft(u32_Len) would result in data loss!!
		pi_RecvMem->DeleteLeft(u32_Len);
		break;
	}
}

void CTCPServer::OnClose(SOCKET s)
{

}
bool CTCPServer::Run()
{
	DWORD dwBindIP = inet_addr(m_strLocalIP);
	USHORT uPort = m_u16LocalPort;
	return StartListen(dwBindIP, uPort);
}
void CTCPServer::Stop()
{
	CloseSockets();
	if (m_bRunning)
	{
		m_bRunning = FALSE;
		WaitForSingleObject(m_hThread, INFINITE);
	}
}

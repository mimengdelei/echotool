
// echoTool.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "echoTool.h"
#include "echoToolDlg.h"
#include "ZZHLog.h"
#include "AutoLock/AutoLock.h"
#include "TAppINIConfig.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CechoToolApp

BEGIN_MESSAGE_MAP(CechoToolApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CechoToolApp construction

CechoToolApp::CechoToolApp()
{
	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CechoToolApp object

CechoToolApp theApp;


// CechoToolApp initialization

BOOL CechoToolApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();


	AfxEnableControlContainer();

	// Create the shell manager, in case the dialog contains
	// any shell tree view or shell list view controls.
	CShellManager *pShellManager = new CShellManager;

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	::SetCurrentDirectory(GetExePath());
	ZZH_LogInit(NULL);
	ZZH_SetLogFun(CechoToolApp::Log);
	ZZH_LOG_INFO(LOGNAME, "start run");

	//创建目录
	CString strExePath = GetExePath();

	::SHCreateDirectoryEx(NULL, strExePath+_T("//bin"), NULL);
	::SHCreateDirectoryEx(NULL, strExePath+_T("//config"), NULL);
	::SHCreateDirectoryEx(NULL, strExePath+_T("//log"), NULL);

	CString strConfig = GetExePath() + "\\config\\echotool.ini";
	if (!theAppINIConfig.Load(strConfig.GetBuffer())){
		theAppINIConfig.Create(strConfig.GetBuffer());
	}
	m_CTCPServer.Init();
	m_CTCPServer.Run();

	m_CUDPServer.Init();
	m_CUDPServer.Run();


	CechoToolDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Delete the shell manager created above.
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}
	ZZH_LOG_INFO(LOGNAME, "exit");
	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

BOOL CechoToolApp::GetLogLine(std::queue<std::string> &logQueue)
{
	std::queue<std::string> empty;
	logQueue.swap(empty);
	CAutoLock lock(m_csLogQueue);
	if (!m_strLogQueue.empty())
	{
		std::swap(m_strLogQueue, logQueue);
		return TRUE;
	}
	return FALSE;
}
BOOL CechoToolApp::OutputLog(std::string strLine)
{
#define MAX_LOG_LINES_500	500
	if (m_bExitApp) return FALSE;
	CAutoLock lock(m_csLogQueue);
	m_strLogQueue.push(strLine);
	if (m_strLogQueue.size() > MAX_LOG_LINES_500)
		m_strLogQueue.pop();
	return TRUE;
}

void CechoToolApp::Log(int logLevel, std::string &strInfo)
{
	SYSTEMTIME sysTime;
	GetLocalTime(&sysTime);

	CString strTime;
	strTime.Format(_T("时间[%02d:%02d:%02d.%03d]:%s"),
		sysTime.wHour,sysTime.wMinute,sysTime.wSecond,sysTime.wMilliseconds,
		strInfo.c_str());
	theApp.OutputLog(strTime.GetBuffer());
}